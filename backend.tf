terraform {
  backend "s3" {
    bucket = "terraformcicdbucket"
    key    = "state"
    region = "us-east-1"
    dynamodb_table = "backend"
  }
}
